﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NunitConsoleApp.Tests
{
    [TestFixture]
    public class CalculatorTests
    {
        [Test]
        public void JDRMultiplyIsCorrect()
        {
            var calc = new Calculator();
            var result = calc.Multiply(9, 3);

            Assert.AreEqual(27,result);
        }
        [Test]
        public void JDRMultiplyIsNotCorrect()
        {
            var calc = new Calculator();
            var result = calc.Multiply(9, 3);

            Assert.AreNotEqual(40, result);
        }
        [Ignore]
        [Test]
        public void JDRMultiplyIsMultFails()
        {
            var calc = new Calculator();
            var result = calc.Multiply(9, 3);

            Assert.AreEqual(100, result);
        }

        [TestCase(9,3, Result = 27)]
        [TestCase(9,2, Result = 18)]
        [TestCase(9,1, Result = 9)]
        public int MultiplyGoodValues(int d1, int d2)
        {
            var calc = new Calculator();
            return calc.Multiply(d1, d2);

        }

        [Test]
        public void AddIsCorrect()
        {
            var calc = new Calculator();
            var result = calc.Add(5, 6);

            Assert.AreEqual(11, result);
        }

        [Test]
        public void SubtractIsCorrect()
        {
            var calc = new Calculator();
            var result = calc.Subtract(5, 6);

            Assert.AreEqual(-1, result);
        }

        [Test]
        public void DivideIsCorrect()
        {
            var calc = new Calculator();
            var result = calc.Divide(10, 2);

            Assert.AreEqual(5, result);
        }

    }
}
