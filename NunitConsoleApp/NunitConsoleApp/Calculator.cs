﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace NunitConsoleApp
{
    public class Calculator
    {
        public int Multiply(int digit1, int digit2)
        {
            return digit1*digit2;
        }
        //------------------------------------- 
        public int Subtract(int sdigit1, int sdigit2)
        {
            return sdigit1 - sdigit2;
        }
        //------------------------------------- 
        public int Add(int adigit1, int adigit2)
        {
            return adigit1 + adigit2;
        }
        //------------------------------------- 
        public int Divide(int ddigit1, int ddigit2)
        {
            return ddigit1 / ddigit2;
        }
    }
}
